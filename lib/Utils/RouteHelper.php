<?php

namespace Molotov\Utils;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Pluralizer;
use Illuminate\Support\Str;

class RouteHelper
{
    public static function resource($slug, $controller = null, $except = []) {
        $camelSlug = Str::camel(str_replace('-', '_', $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $controller = $controller ?: ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(
            function () use ($controller, $pluralSlug, $except) {
                if (!in_array('index', $except)) {
                    static::indexRoute($controller, $pluralSlug);
                }

                if (!in_array('create', $except)) {
                    Route::post('/', "$controller@create")
                        ->name("$pluralSlug.create");
                }

                if (!in_array('restore', $except)) {
                    Route::put('/{id}/restore', "$controller@restore")
                        ->name("$pluralSlug.restore");
                }

                if (!in_array('retrieve', $except)) {
                    static::retrieveRoute($controller, $pluralSlug);
                }

                if (!in_array('update', $except)) {
                    Route::put('/{id}', "$controller@update")
                        ->name("$pluralSlug.update");
                }

                if (!in_array('destroy', $except)) {
                    Route::delete('/{id}', "$controller@destroy")
                        ->name("$pluralSlug.destroy");
                }

                if (!in_array('template', $except)) {
                    Route::options('/', "$controller@template")
                        ->name("$pluralSlug.template");
                }
            }
        );
    }

    public static function index($slug) {
        $camelSlug = Str::camel(str_replace('-', '_', $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $controller = ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(
            function () use ($controller, $pluralSlug) {
                static::indexRoute($controller, $pluralSlug);
            }
        );
    }

    private static function indexRoute($controller, $pluralSlug) {
        Route::get('/', "$controller@index")
            ->name("$pluralSlug.index");
    }

    public static function retrieve($slug) {
        $camelSlug = Str::camel(str_replace('-', '_', $slug));
        $pluralSlug = Pluralizer::plural($slug);

        $controller = ucfirst("{$camelSlug}Controller");

        Route::prefix($pluralSlug)->group(
            function () use ($controller, $pluralSlug) {
                static::retrieveRoute($controller, $pluralSlug);
            }
        );
    }

    private static function retrieveRoute($controller, $pluralSlug) {
        Route::get('/{id}', "$controller@retrieve")
            ->name("$pluralSlug.retrieve");
    }

    public static function subresource($parentSlug, $slug, $controller = null, $except = []) {
        $parentCamelSlug = Str::camel(str_replace('-', '_', $parentSlug));
        $parentPluralSlug = Pluralizer::plural($parentSlug);

        $pluralSlug = Pluralizer::plural($slug);

        $controller = $controller ?: ucfirst("{$parentCamelSlug}Controller");
        $methodSuffix = ucfirst(Str::camel(Pluralizer::plural($slug)));

        if (!in_array('index', $except)) {
            Route::get(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug",
                "$controller@index{$methodSuffix}"
            )->name("{$parentPluralSlug}_{$pluralSlug}.index");
        }

        if (!in_array('create', $except)) {
            Route::post(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug",
                "$controller@create{$methodSuffix}"
            )->name("{$parentPluralSlug}_{$pluralSlug}.create");
        }
        if (!in_array('retrieve', $except)) {
            Route::get(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}",
                "$controller@retrieve{$methodSuffix}"
            )->name("{$parentPluralSlug}_{$pluralSlug}.retrieve");
        }
        if (!in_array('update', $except)) {
            Route::put(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}",
                "$controller@update{$methodSuffix}"
            )->name("{$parentPluralSlug}_{$pluralSlug}.update");
        }
        if (!in_array('destroy', $except)) {
            Route::delete(
                "/$parentPluralSlug/{{$parentSlug}_id}/$pluralSlug/{{$slug}_id}",
                "$controller@destroy{$methodSuffix}"
            )->name("{$parentPluralSlug}_{$pluralSlug}.destroy");
        }
    }
}
