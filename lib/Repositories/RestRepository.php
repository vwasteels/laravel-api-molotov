<?php

namespace Molotov\Repositories;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class RestRepository
{
    protected $model;
    protected $columnsDefinition = [];

    protected $reserved = [
        '!fields',
        'fields',
        'order',
        'page',
        'per_page',
        'q',
    ];

    public function get($request) {
        $query = $this->model;

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $params = $this->parseQueryString(array_get($_SERVER, 'QUERY_STRING')) ?: $request->all();
        foreach ($params as $param => $value) {
            if (in_array($param, $this->reserved)) {
                continue;
            }

            $query = $this->applyFilter($this->model, $param, $value, $query, $request);
        }

        if (isset($params['q'])) {
            $query = $query->search($params['q']);
        }

        if (isset($params['order'])) {
            $query = $this->orderBy($query, $params['order']);
        }

        if ($fields = $request->getFields()) {
            $this->applyWithFromQuery($fields, $query);
        }

        $columns = $this->columnsDefinition;
        foreach ($columns as $column) {
            $query = $column($query);
        }

        // TODO Move to controller
        $perPage = $request->get('per_page') ?: 10;
        $page = $request->get('page') ?: 1;

        if (strpos($query->toSql(), 'group by') !== false) {
            $total = $this->wrapQueryForRealTotal($query);
        } else {
            try {
                $total = $query->count();
            } catch (\Illuminate\Database\QueryException $e) {
                $total = $this->wrapQueryForRealTotal($query);
            }
        }

        try {
            $items = $perPage == -1 ? $query->get() :
              $query->take($perPage)->skip($perPage * ($page - 1))->get()

            return [
              'items' => $items,
              'total' => $total
            ];
        } catch (RelationNotFoundException $e) {
            throw new ValidationException([
                'includes' => 'relationship does not exist',
            ]);
        }
    }

    public function find($request, $id) {
        if (!intval($id)) {
            return abort(422, 'Numeric ids.');
        }

        $query = $this->model;

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $params = $request->all();
        foreach ($params as $param => $value) {
            if (in_array($param, $this->reserved)) {
                continue;
            }

            $query = $this->applyFilter($this->model, $param, $value, $query, $request);
        }

        if ($fields = $request->getFields()) {
            $this->applyWithFromQuery($fields, $query);
        }

        $columns = $this->columnsDefinition;
        foreach ($columns as $column) {
            $query = $column($query);
        }

        return $query->findOrFail($id);
    }

    public function create($data) {
        $this->model->fill($data);

        $this->model->save();

        $this->saveRelations($data);

        $this->model->save();

        return $this->model;
    }

    public function update($request, $id, $data) {
        $query = $this->model;

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->fill($data);

        $this->model->save();

        $this->saveRelations($data);

        $this->model->save();

        return $this->model->find($id);
    }

    public function destroy($request, $id) {
        $query = $this->model;

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->delete();

        return $this->model->fresh();
    }

    public function restore($request, $id) {
        $query = $this->model->withTrashed();

        if (method_exists($query, 'scopeAccessibleBy')) {
            $query = $query->accessibleBy($request->user());
        }

        $this->model = $query->findOrFail($id);

        $this->model->restore();

        return $this->model->fresh();
    }

    protected function orderBy($query, $def) {
        $columns = explode(',', $def);

        foreach ($columns as $column) {
            if (strpos($column, '-') === 0) {
                $direction = 'desc';
                $column = substr($column, 1);
            } else {
                $direction = 'asc';
            }

            if (in_array($column, array_keys($this->columnsDefinition))) {
                $query = $query->orderBy($column, $direction);
            } else {
                $table = $this->model->getTable();
                $query = $query->orderBy("$table.$column", $direction);
            }
        }

        return $query;
    }

    protected function saveRelations($data) {
        $this->saveItems($data);
        $this->saveCollections($data);
    }

    protected function saveItems($data) {
        foreach (array_diff(array_keys($data), $this->model->getFillable()) as $field) {
            if ($field === 'id') {
                continue;
            }

            if (preg_match('/_id$/', $field)) {
                $relationName = str_replace('_id', '', $field);

                if (in_array($relationName, $this->model->items)) {
                    $newAssoc = $data[$field];
                    $relation = $this->model->{$relationName}();

                    if (is_a($relation, BelongsTo::class)) {
                        if ($newAssoc) {
                            $relation->associate($data[$field]);
                        } else {
                            $relation->dissociate();
                        }
                    } else {
                        if ($newAssoc) {
                            $newData = [ 'id' => $newAssoc ];
                            $this->saveRelatedItem($this->model, $relationName, $newData);
                        } else {
                            $this->deleteRelatedItem($this->model, $relationName);
                        }
                    }
                }
            } elseif (in_array($field, $this->model->items) && is_array($data[$field])) {
                $related = $this->saveRelatedItem($this->model, $field, $data[$field]);
                $relation = $this->model->{$field}();

                if ($this->model->{$field}
                    && $this->model->{$field}->id !== $related->id) {
                    if (is_a($relation, HasOne::class)) {
                        $relation->delete();
                    } else {
                        $relation->dissociate();
                    }
                }

                foreach (array_keys($related->morphOnes) as $morphOne) {
                    $this->savePolymorphicRelation($related, $morphOne, $data[$field]);
                }

                if (is_a($relation, BelongsTo::class)) {
                    $relation->associate($related);
                }
            } elseif (in_array($field, array_keys($this->model->morphOnes))
                && array_key_exists($field, $data)) {
                if (is_array($data[$field]) && !!$data[$field]) {
                    if ($this->model->{$field}
                        && $this->model->{$field}->id !== $data[$field]['id']) {
                        $this->model->{$field}->delete();
                    }

                    $this->savePolymorphicRelation($this->model, $field, $data);
                } else {
                    $this->deletePolymorphicRelation($this->model, $field);
                }
            }
        }
    }

    protected function saveCollections($data) {
        foreach (array_diff(array_keys($data), $this->model->getFillable()) as $field) {
            if ($field === 'id') {
                continue;
            }

            if (array_key_exists($field, $data)) {
                if (is_array($data[$field])) {
                    $allowedRelations = array_merge(
                        $this->model->collections,
                        array_keys($this->model->morphManys)
                    );
                    if (in_array($field, $allowedRelations)) {
                        $relation = $this->model->{$field}();

                        if (is_a($relation, HasManyThrough::class)) {
                            continue;
                        }

                        $ids = [];

                        if (method_exists($relation, 'getPivotClass')) {
                            $pivotClass = $relation->getPivotClass();
                            $pivot = new $pivotClass;
                            $pivotAttributes = $pivot->getFillable();
                            $pivotItems = array_merge($pivot->items, array_keys($pivot->morphOnes));

                            foreach ($data[$field] as $element) {
                                $pivotData = [];
                                $pivotItemData = [];

                                foreach ($pivotAttributes as $pivotAttribute) {
                                    if (!array_key_exists($pivotAttribute, $element)) {
                                        continue;
                                    }

                                    $pivotData[$pivotAttribute] = $element[$pivotAttribute];
                                    unset($element[$pivotAttribute]);
                                }

                                foreach ($pivotItems as $pivotItem) {
                                    if (!array_key_exists($pivotItem, $element)) {
                                        continue;
                                    }

                                    $pivotItemData[$pivotItem] = $element[$pivotItem];
                                    unset($element[$pivotItem]);
                                }

                                if (array_key_exists('id', $element) && $element['id']) {
                                    $sync = [];
                                    $sync[$element['id']] = $pivotData;
                                    $relation->syncWithoutDetaching($sync);

                                    $targetPivot = $this->model->{$field}()
                                        ->find($element['id'])
                                        ->pivot;

                                    foreach ($pivotItems as $pivotItem) {
                                        $this->savePolymorphicRelation(
                                            $targetPivot,
                                            $pivotItem,
                                            $pivotItemData
                                        );
                                    }

                                    $ids[] = $element['id'];
                                }
                            }
                        } else {
                            foreach ($data[$field] as $element) {
                                if (array_key_exists('id', $element) && $element['id']) {
                                    $ids[] = $element['id'];
                                }
                            }
                        }

                        $relatedClass = $relation->getRelated();

                        if ($relatedClass->readOnly) {
                            continue;
                        }

                        if (is_a($relation, MorphMany::class)) {
                            foreach ($ids as $id) {
                                $item = $relatedClass->find($id);
                                $relation->save($id);
                            }
                            $relation->sync($ids);
                        } elseif (is_a($relation, HasMany::class)) {
                            $newItems = [];
                            foreach ($data[$field] as $element) {
                                if (array_key_exists('id', $element) && $element['id']) {
                                    $existingItem = $relatedClass->find($element['id']);
                                    $existingItem->fill($element);
                                    $newItems[] = $existingItem;
                                } else {
                                    $newItem = new $relatedClass;
                                    $newItem->fill($element);
                                    $newItems[] = $newItem;
                                }
                            }

                            $existingIds = $relation->get()->pluck('id')->toArray();
                            $removedIds = array_diff($existingIds, $ids);

                            foreach ($removedIds as $id) {
                                $existingItem = $relation->find($id);
                                if ($existingItem) {
                                    $existingItem->delete();
                                }
                            }

                            $relation->saveMany($newItems);
                        } else {
                            $relation->sync($ids);
                        }
                    }
                }
            }
        }
    }

    protected function applyFilter(&$model, $param, $value, $query, $request) {
        $negative = $param[0] === '!';
        $paramName = str_replace('!', '', $param);

        if (strpos($paramName, '.') !== false) {
            [$relation, $field] = explode('.', $paramName, 2);

            if (in_array($relation, array_merge($model->collections, $model->items))) {
                $targetModel = $model->{$relation}()->getRelated();
                $fieldQuery = $negative ? "!$field" : $field;
                return $query->whereHas(
                    $relation,
                    function ($q) use ($targetModel, $fieldQuery, $value, $request) {
                        return $this->applyFilter(
                            $targetModel,
                            $fieldQuery,
                            $value,
                            $q,
                            $request
                        );
                    }
                );
            }

            return $query;
        }

        if (in_array($paramName, $this->model->items)) {
            if ($negative) {
                return $query->doesntHave($paramName);
            }

            return $query->whereHas($paramName);
        }

        $columnsDefinition = $model::getColumnsDefinition();
        if (in_array($paramName, array_keys($columnsDefinition))) {
            $query = $columnsDefinition[$paramName]($query);
            $scopedParam = \DB::raw($columnsDefinition[$paramName]());
        } else {
            $scopedParam = "{$query->getModel()->getTable()}.$paramName";
        }

        if ($paramName === 'id' && $value === 'me') {
            $value = $request->user()->id;
        }

        if ($paramName === 'deleted_at' && !!$value) {
            $query = $query->withTrashed();
        }

        // If a type is defined for this filter, use the query
        // language, otherwise fallback to default Laravel filtering
        $filterType = array_get($model::$filterTypes, $paramName, 'default');
        if (is_array($filterType)) {
            $filterType = 'enum';
        }
        switch ($filterType) {
            case 'enum':
                if ($negative) {
                    return $query->whereNotIn($scopedParam, explode(',', $value));
                }

                return $query->whereIn($scopedParam, explode(',', $value));
            case 'boolean':
                return $query->where(
                    $scopedParam,
                    $value === 'true' || $value === '1' || !$negative
                );
            case 'number':
                if ($value === '') {
                    return $query;
                }

                if (strpos($value, ':') !== false) {
                    return $this->parseRangeFilter($scopedParam, $value, $query);
                }

                $values = array_map(
                    'intval',
                    array_map(
                        'trim',
                        array_filter(
                            explode(',', $value),
                            function ($i) {
                                return !!$i;
                            }
                        )
                    )
                );

                if ($negative) {
                    return $query->whereNotIn($scopedParam, $values);
                }

                return $query->whereIn($scopedParam, $values);
                break;
            case 'text':
                return $query->where(
                    \DB::raw("unaccent($scopedParam)"),
                    'ILIKE',
                    \DB::raw("unaccent('%$value%')")
                );
                break;
            case 'date':
                if ($value === '') {
                    return $query;
                }

                if (strpos($value, ':') !== false) {
                    return $this->parseRangeFilter($scopedParam, $value, $query);
                }

                return $query->where($scopedParam, '>=', $value);
                break;
            default:
                if ($negative) {
                    return $query->where($scopedParam, '!=', $value);
                }

                return $query->where($scopedParam, $value);
                break;
        }

        return $query;
    }

    protected function applyWithFromQuery($fields, &$query) {
        foreach (array_keys($fields) as $field) {
            $relations = array_merge(
                $this->model->items,
                $this->model->collections,
                array_keys($this->model->morphOnes)
            );
            if (in_array($field, $relations)) {
                $query = $query->with(Str::camel($field));
            }
        }
    }

    protected function deletePolymorphicRelation(&$model, $field) {
        if ($currentRelation = $model->{$field}()->first()) {
            $currentRelation->delete();
        }

        return $currentRelation;
    }

    protected function savePolymorphicRelation(&$model, $field, &$data) {
        if (!array_key_exists($field, $data)) {
            return $model->{$field};
        }

        if (array_key_exists($field, $data)) {
            if (!$data[$field]) {
                if ($model->{$field}) {
                    $model->{$field}->delete();
                }

                return null;
            }
        }

        if (array_key_exists('id', $data[$field])) {
            $newRelation = $model->{$field}()
                ->getRelated()->find($data[$field]['id']);
        } else {
            $newRelation = $model->{$field}()->getRelated();
        }

        if (!$newRelation) {
            return null;
        }

        $morphId = $model->morphOnes[$field] . '_id';
        $morphType = $model->morphOnes[$field] . '_type';

        $newRelation->{$morphId} = $model->id;
        $newRelation->{$morphType} = get_class($model);

        $newRelation->save();

        return $newRelation;
    }

    protected function deleteRelatedItem(&$model, $field) {
        $relation = $model->{$field}();
        $related = $model->{$field};

        if (!$related) {
            return $related;
        }

        if (is_a($relation, HasOne::class)) {
            $related->{$relation->getForeignKeyName()} = null;
        }

        $related->save();

        return $related;
    }

    protected function saveRelatedItem(&$model, $field, &$data) {
        $relation = $model->{$field}();
        if (!array_key_exists('id', $data)) {
            $related = $relation->getRelated();
        } else {
            $related = $relation->getRelated()->find($data['id']);
        }

        if ($related->readOnly) {
            return $related;
        }

        $relatedData = array_intersect_key(
            $data,
            array_flip($related->getFillable())
        );

        if (is_a($relation, HasOne::class)) {
            $related->{$relation->getForeignKeyName()} = $model->id;
        }

        $related->fill($relatedData);
        $related->save();

        return $related;
    }

    protected function parseRangeFilter($scopedParam, $value, &$query) {
        [$start, $end] = array_map('trim', (explode(':', $value)));

        if (!$start && !$end) {
            return $query;
        } elseif (!$start) {
            return $query->where($scopedParam, '<=', $end);
        } elseif (!$end) {
            return $query->where($scopedParam, '>=', $start);
        }

        return $query->whereBetween($scopedParam, [$start, $end]);
    }

    // PHP's default query string parser replaces . by _
    // which would break our query language
    private function parseQueryString($data) {
        $data = preg_replace_callback('/(?:^|(?<=&))[^=[]+/', function ($match) {
            return bin2hex(urldecode($match[0]));
        }, $data);

        parse_str($data, $values);

        return array_combine(array_map('hex2bin', array_keys($values)), $values);
    }

    private function wrapQueryForRealTotal($query) {
        $sql = $query->toSql(); // FIXME Virtual columns will mess with count

        $bindings = array_map(function ($p) {
            $escapedP = pg_escape_string($p);
            return "'$escapedP'";
        }, $query->getBindings());

        $sql = str_replace('%', '~!~!~', $sql);
        $sql = str_replace('?', '%s', $sql);
        $subquery = vsprintf($sql, $bindings);
        $subquery = str_replace('~!~!~', '%', $subquery);

        return \DB::select(
            \DB::raw("SELECT COUNT(*) AS cnt FROM ($subquery) AS query")
        )[0]->cnt;
    }
}
