<?php

namespace Molotov\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class WrapInTransaction
{
    public function handle($request, Closure $next, $guard = null) {
        DB::beginTransaction();

        $response = $next($request);

        DB::commit();

        return $response;
    }
}
