<?php

namespace Molotov\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;
use Molotov\Traits\RespondsWithErrors;
use Symfony\Component\HttpFoundation\StreamedResponse;

class RestController extends Controller
{
    use RespondsWithErrors;

    protected $repo;
    protected $model;

    protected function respondWithCollection($request, $items, $total) {
        $perPage = $request->get('per_page') ?: 10;
        $page = $request->get('page') ?: 1;

        if ($items->isEmpty()) {
            return new LengthAwarePaginator([], $total, $perPage, $page);
        }

        $results = $this->transformCollection($request, $items);

        return new LengthAwarePaginator($results, $total, $perPage, $page);
    }

    protected function respondWithCsv($request, $items, $model) {
        $export = new $items[0]::$export(
            $this->transformCollection($request, $items),
            explode(',', $request->get('fields')),
            $model
        );

        $userId = $request->user()->id;
        $date = date('YmdHmi');
        $modelClassName = get_class($model);
        $modelClassNameParts = explode('\\', $modelClassName);
        $modelName = array_pop($modelClassNameParts);
        $filename = Str::plural(strtolower($modelName));

        $path = "/exports/$date.$userId.$filename.csv";

        Excel::store($export, $path, 'local');

        return $path;
    }

    protected function validateAndCreate($request) {
        $input = $request->json()->all();

        $validator = Validator::make(
            $input,
            $this->model::getRules('create', $request->user()),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $item = $this->repo->create($input);

        $className = get_class($request);
        $fieldsRequest = new $className;
        $fieldsRequest->setUserResolver($request->getUserResolver());
        $fieldsRequest->merge([ 'fields' => $request->get('fields') ]);

        return $this->repo->find($fieldsRequest, $item->id);
    }

    protected function respondWithItem($request, $item, $status = 200) {
        $result = $this->transformItem($request, $item);

        return response($result, $status);
    }

    protected function validateAndUpdate($request, $id) {
        $input = $request->json()->all();
        $item = $this->model->findOrFail($id);
        $item->fill($input);

        $validator = Validator::make(
            $item->toArray(),
            $this->model::getRules('update', $request->user()),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        $item = $this->repo->update($request, $id, $input);

        $className = get_class($request);
        $fieldsRequest = new $className;
        $fieldsRequest->setUserResolver($request->getUserResolver());
        $fieldsRequest->merge([ 'fields' => $request->get('fields') ]);

        return $this->repo->find($fieldsRequest, $item->id);
    }

    protected function validateAndDestroy($request, $id) {
        $input = $request->json()->all();

        $validator = Validator::make(
            $input,
            $this->model::getRules('destroy', $request->user()),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->repo->destroy($request, $id);
    }

    protected function validateAndRestore($request, $id) {
        $input = $request->json()->all();

        $validator = Validator::make(
            $input,
            $this->model::getRules('destroy', $request->user()),
            $this->model::$validationMessages
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $this->repo->restore($request, $id);
    }

    protected function streamFile($callback, $headers) {
        $response = new StreamedResponse($callback, 200, $headers);
        $response->send();
    }

    protected function formatRules($rules) {
        if (is_string($rules)) {
            $rules = explode('|', $rules);
        }

        $cleanRules = [];
        foreach ($rules as $rule) {
            if (!is_string($rule)) {
                continue;
            }
            $cleanRules[] = $rule;
        }

        $flipRules = array_flip($cleanRules);

        foreach ($flipRules as $key => $rule) {
            if (strpos($key, 'in:') === 0) {
                unset($flipRules[$key]);
                [$_, $values] = explode(':', $key);
                $flipRules['oneOf'] = explode(',', $values);
            } elseif (strpos($key, ':')) {
                unset($flipRules[$key]);

                [$rule, $values] = explode(':', $key);
                $flipRules[$rule] = explode(',', $values);
                if (count($flipRules[$rule]) === 1) {
                    $flipRules[$rule] = array_pop($flipRules[$rule]);
                }

                switch ($rule) {
                    case 'regex':
                        $flipRules[$rule] = substr($flipRules[$rule], 1, -1);
                        break;
                    default:
                        break;
                }
            } else {
                $flipRules[$key] = true;
            }
        }

        return $flipRules;
    }

    protected function transformCollection($request, $items) {
        $transformer = new $items[0]::$transformer;
        return $items->map(function ($item) use ($transformer, $request) {
            return $transformer->transform($item, [
                'fields' => $request->getFields(),
                '!fields' => $request->getNotFields(),
            ]);
        });
    }

    protected function transformItem($request, $item) {
        $transformer = new $item::$transformer;

        $reflect = new \ReflectionClass($this->model);
        $shortName = $reflect->getShortName();
        $context = [];
        $context[$shortName] = true;

        return $transformer->transform($item, [
            'fields' => $request->getFields(),
            '!fields' => $request->getNotFields(),
            'pivot' => isset($item->pivot) ? $item->pivot->toArray() : null,
            'context' => $context,
        ]);
    }
}
