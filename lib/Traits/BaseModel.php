<?php

namespace Molotov\Traits;

use Molotov\Exports\BaseExport;
use Molotov\Transformers\BaseTransformer;
use Illuminate\Database\Eloquent\Builder;

trait BaseModel
{
    public static $filterTypes = [];

    public static $export = BaseExport::class;

    public static $transformer = BaseTransformer::class;

    public static function getColumnsDefinition() {
        return [];
    }

    public static function getRules($action = '', $auth = null) {
        switch ($action) {
            case 'destroy':
                return [];
            default:
                return static::$rules;
        }
    }

    public static $rules = [];

    public static $validationMessages = [];

    protected $appends = [];

    public $readOnly = false;

    public $items = [];

    public $collections = [];

    public $computed = [];

    public $morphOnes = [];

    public $morphManys = [];

    public function getWith() {
        return $this->with;
    }

    public function belongsTo($related, $foreignKey = null, $ownerKey = null, $relation = null) {
        if (is_null($relation)) {
            $relation = $this->guessBelongsToRelation();
        }

        $query = parent::belongsTo($related, $foreignKey, $ownerKey, $relation);

        $columns = $related::getColumnsDefinition();
        foreach ($columns as $column) {
            $query = $column($query);
        }

        return $query;
    }

    // FIXME Doesn't add the custom keys on the resulting object
    public function belongsToMany(
        $related,
        $table = null,
        $foreignPivotKey = null,
        $relatedPivotKey = null,
        $parentKey = null,
        $relatedKey = null,
        $relation = null
    ) {
        $instance = $this->newRelatedInstance($related);

        $foreignPivotKey = $foreignPivotKey ?: $this->getForeignKey();

        $relatedPivotKey = $relatedPivotKey ?: $instance->getForeignKey();

        if (is_null($table)) {
            $table = $this->joiningTable($related, $instance);
        }

        $query = $instance->newQuery();

        $columns = $related::getColumnsDefinition();
        foreach ($columns as $column) {
            $query = $column($query);
        }

        return $this->newBelongsToMany(
            $query,
            $this,
            $table,
            $foreignPivotKey,
            $relatedPivotKey,
            $parentKey ?: $this->getKeyName(),
            $relatedKey ?: $instance->getKeyName(),
            $relation
        );
    }

    public function scopeSearch(Builder $query, $q) {
        return $query;
    }
}
