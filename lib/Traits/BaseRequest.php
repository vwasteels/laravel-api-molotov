<?php

namespace Molotov\Traits;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

trait BaseRequest
{
    use ParsesFields;

    protected static function rebaseRules($scope, $rules) {
        return array_reduce(
            array_keys($rules),
            function ($acc, $key) use ($rules, $scope) {
                $acc[$scope . '.' . $key] = $rules[$key];
                return $acc;
            },
            []
        );
    }

    private $fieldsMemo = null;
    private $notFieldsMemo = null;

    public function authorize() {
        return true;
    }

    public function rules() {
        return [];
    }

    public function message() {
        return [];
    }

    public function getFields() {
        if ($this->fieldsMemo !== null) {
            return $this->fieldsMemo;
        }

        if (!$this->get('fields')) {
            return $this->fieldsMemo = ['*' => '*'];
        }

        return $this->fieldsMemo = $this->parseFields(
            $this->splitFields($this->get('fields'))
        );
    }

    public function getNotFields() {
        if ($this->notFieldsMemo !== null) {
            return $this->notFieldsMemo;
        }

        if (!$this->get('!fields')) {
            return $this->notFieldsMemo = [];
        }

        return $this->notFieldsMemo = $this->parseFields(
            $this->splitFields($this->get('!fields'))
        );
    }

    public function redirectAuth($requestClass) {
        $newRequest = new $requestClass;
        $newRequest->setUserResolver(function () {
            return $this->user();
        });
        if (!$newRequest->authorize()) {
            return abort(403);
        }
        return $newRequest;
    }

    public function redirect($requestClass) {
        $newRequest = new $requestClass;
        $newRequest->setUserResolver(function () {
            return $this->user();
        });
        $newRequest->setRouteResolver(function () {
            return $this->route();
        });
        $newRequest->merge($this->all());

        if (!$newRequest->authorize()) {
            return abort(403);
        }

        $validator = Validator::make(
            $newRequest->all(),
            $newRequest->rules(),
            $newRequest->message()
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }

        return $newRequest;
    }
}
